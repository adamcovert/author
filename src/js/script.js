$(document).ready(function () {

  svg4everybody();


  var promoSwiper = new Swiper('.s-main-page__slider', {
    speed: 750,
    autoplay: {
      delay: 3000,
    },
  })


  $(window).scroll(function () {
    windowScroll();
  }).scroll();

  function windowScroll() {
    var pageScrollY = 0;
    if (typeof (window.pageYOffset) == 'number') pageScrollY = window.pageYOffset;
    else pageScrollY = document.documentElement.scrollTop;

    if (pageScrollY > 20) {
      $('.s-page').addClass('s-page--is-scrolling');
    } else {
      $('.s-page').removeClass('s-page--is-scrolling');
    }
  };


  $('.s-main-page__toggle-btn').on('click', function () {
    $('.s-main-page__project').toggleClass('s-main-page__project--is-open')
  });


  $('.s-hamburger').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-open');
    bodyScrollLock.disableBodyScroll();
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-open');
    bodyScrollLock.enableBodyScroll();
  });


  function expand() {
    $('.s-page-header__search-form button').toggleClass('close');
    $('.s-page-header__search-form label').toggleClass('open');
    if ($('.s-page-header__search-form button').hasClass('close')) {
      $('.s-page-header__search-form input').focus();
    } else {
      $('.s-page-header__search-form input').blur();
    }
  };

  $('.s-page-header__search-form button').on('click', expand);


  if (document.getElementById('to-top')) {
    document.getElementById('to-top').addEventListener('click', function (e) {
      e.preventDefault();
      var scroll = window.pageYOffset;
      var targetTop = 0;
      var scrollDiff = (scroll - targetTop) * -1;
      animate({
        duration: 500,
        timing: function (timeFraction) {
          return Math.pow(timeFraction, 4);
        },
        draw: function (progress) {
          var scrollNow = scroll + progress * scrollDiff;
          window.scrollTo(0, scrollNow);
        }
      });
    }, false);
    window.addEventListener('scroll', visibilityToggle);
    visibilityToggle();
  };

  function visibilityToggle() {
    if (window.pageYOffset >= 500) {
      document.getElementById('to-top').classList.add('s-to-top--visible');
    } else {
      document.getElementById('to-top').classList.remove('s-to-top--visible');
    }
  };

  function animate(_ref) {
    var timing = _ref.timing,
      draw = _ref.draw,
      duration = _ref.duration;
    var start = performance.now();
    requestAnimationFrame(function animate(time) {
      var timeFraction = (time - start) / duration;
      if (timeFraction > 1) timeFraction = 1;
      var progress = timing(timeFraction);
      draw(progress);
      if (timeFraction < 1) {
        requestAnimationFrame(animate);
      }
    });
  };


  var swiperTeam = new Swiper('.s-team__slider .swiper-container', {
    spaceBetween: 30,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      556: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 3
      }
    }
  });


  var swiperRealizedProjects = new Swiper('.s-realized-projects__slider', {
    slidesPerView: 'auto',
    spaceBetween: 5,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      320: {
        slidesPerView: 2
      },
      480: {
        slidesPerView: 3
      },
      640: {
        slidesPerView: 4
      }
    }
  });

  $('.s-realized-projects__slider').lightGallery({
    selector: '.s-realized-projects__item',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });


  var swiperNewsItem = new Swiper('.s-news-item__slider .swiper-container', {
    slidesPerView: 1,
    spaceBetween: 5,
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction'
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  $('.s-news-item__slider').lightGallery({
    selector: '.s-news-item__slide',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });



  $('.s-project__gallery-list').lightGallery({
    selector: '.s-project__gallery-card',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });

  $('.s-project__cover').on('click', function () {
    $('.s-project__gallery-card').trigger('click');
  });



  $('.s-testimonial__more-btn').on('click', function () {
    var button = $(this);
    var text = button.text() == 'Посмотреть полностью' ? 'Скрыть' : 'Посмотреть полностью';
    button.text(text);
  });



  $('.s-projects__filter-list > li').each(function () {
    $(this).children().each(function () {
      if ($(this).hasClass('children')) {
        $(this).parent().addClass('has-child')
      }
    });
  });

  $('.s-projects__filter-list .has-child').each(function () {
    $(this).children('a').on('click', function (e) {
      e.preventDefault(e);
      $('.s-projects__filter-list .has-child').each(function () {
        $(this).children('ul').removeClass('is-open');
      })
      $(this).next('ul').addClass('is-open');
      $(this).addClass('is-active');
      $(this).parent().parent().addClass('sub-menu-is-open');
    })
  })
});